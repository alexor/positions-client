import { observable, action, computed } from 'mobx';
import serverPositions from '../Services/sourceService';

class PositionStore {
    @observable positions = [];
    
    fetchPositions = () => {       
        serverPositions.getPositions().then((results) => {          
            this.positions = results;
        });
    };    
       
    @action
    addPosition = position => {
   
        this.positions.push(position);
        serverPositions.addPosition(position);
    };
  
    @action
    deletePosition = index => {
        this.positions.splice(index,1);
        serverPositions.deletePosition(index);      
    }   
    
}

const singleton = new PositionStore();

export default singleton;