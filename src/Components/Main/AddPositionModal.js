import React from 'react';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import classes from './AddPositionModal.css';
import { inject, observer } from 'mobx-react';


const clearProps = {
  open: false,
  long: '',
  lat: '',
  longError: true,
  latError: true
}

@inject('PositionStore')
@observer
class AddPositionModal extends React.Component {
  state = clearProps;

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen) {
      this.setState({
        open: nextProps.isOpen
      });
    }
  }

  onAddClick = () => {
    debugger;
    const { PositionStore } = this.props;
    let newPosition = {
        lat: Number(this.state.lat),
        long: Number(this.state.long)
    }
    
    if (this.validatePosition(newPosition))
    {
      PositionStore.addPosition(newPosition);
      this.setState({ open: false });
      this.props.onClose();
    }
    
  };


  onCancelClick = () => {
    this.setState(clearProps);
    this.props.onClose();
  };

  validatePosition = (newPosition) =>{
    let vLat = /^(-?[1-8]?\d(?:\.\d{1,18})?|90(?:\.0{1,18})?)$/;
    let vlong = /^(-?(?:1[0-7]|[1-9])?\d(?:\.\d{1,18})?|180(?:\.0{1,18})?)$/;    
    
    var validLat = vLat.test(newPosition.lat);
    var validLon = vlong.test(newPosition.long);    
      
      this.setState({
        latError: validLat,
        longError: validLon
      });    
    
      if(validLat && validLon) {
          return true;
      } else {
          return false;
      }
  }
  

  render() {
    const {longError, latError} = this.state;
   debugger;
    return <div>
        <Dialog
          open={this.state.open}          
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Add Position"}</DialogTitle>
          <DialogContent>
              <div className={classes.textFieldWrapper}>
              <TextField
                    error={!longError}                            
                    label="Latitude"
                    className={classes.textField}
                    placeholder="Latitude"                  
                    onChange = {(e) => this.setState({'lat': e.target.value})}
                    margin='normal'
                /> 
              <TextField   
                    error={!latError}                    
                    label="Longitude"          
                    className={classes.textField}
                    placeholder="Longitude"                 
                    onChange = {(e) => this.setState({'long': e.target.value})}
                    margin='normal'
                />                           
              </div>        
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onAddClick} color="primary">
              Ok
            </Button>
            <Button onClick={this.onCancelClick} color="primary" autoFocus>
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </div>   
  }
}

export default AddPositionModal;
