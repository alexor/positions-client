import React from 'react';
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import { inject, observer } from 'mobx-react';
import _ from 'lodash';
import AddPositionModal from './AddPositionModal';
import Button from 'material-ui/Button';

const Map = ReactMapboxGl({
  accessToken: "pk.eyJ1IjoiYWxleG9yIiwiYSI6ImNqaDZjNjJ4aTFuY3UzM28xbHo5OHoxaDAifQ.h0KLklqf8NhFiaJBFlcnCQ"
});

const POSITION_CIRCLE_PAINT = {
  'circle-stroke-width': 4,
  'circle-radius': 10,
  'circle-blur': 0.15,
  'circle-color': '#3770C6',
  'circle-stroke-color': 'white'
};

@inject('PositionStore')
@observer
class Main extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
    };
  }

  componentDidMount() {
    const { PositionStore } = this.props;
    PositionStore.fetchPositions();
  }

  addPostionClick() {
    this.setState({
      isModalOpen: true
    })
  }

  deletePosition(index) {
    debugger;
    const { PositionStore } = this.props;
    PositionStore.deletePosition(index);    
  }


  render() {
  
    const { PositionStore } = this.props;
    const centerPosition = PositionStore.positions.length > 0 ?
      [PositionStore.positions[0].long, PositionStore.positions[0].lat] :
      [34.855499,32.109333];
    return <div>
      <Map   
        center={centerPosition}     
        style="mapbox://styles/mapbox/streets-v9"
        containerStyle={{
          height: "500px",
          width: "99vw"
        }}>
        {_.map(PositionStore.positions, (item, index) => {
       
            return <Layer key={index}
                type="circle"
                id={"position-marker-"+index}
                paint={POSITION_CIRCLE_PAINT}>                           
                  <Feature coordinates={[item.long, item.lat]}
                  onClick={() => this.deletePosition(index)} />
            </Layer>
        })}

             
      </Map>
      <div style={{textAlign: 'right'}}>
        <Button variant="raised" color='primary' onClick={(e) => this.addPostionClick()}>Add Marker</Button>
      </div>
      <AddPositionModal isOpen={this.state.isModalOpen} onClose={(e) => { this.setState({ isModalOpen: false }) }}></AddPositionModal>
    </div>
  }

}

export default Main