import React from 'react';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import Main from './Main/Main';
import { Provider } from 'mobx-react';
import PositionStore from '../Stores/PositionStore';


const theme = createMuiTheme();

class App extends React.Component {

    render() { return  <MuiThemeProvider theme={theme}>
            <Provider PositionStore={PositionStore}>
                <Main/>
            </Provider>
        </MuiThemeProvider>        
    }

}

export default App;