const apiServers = {
    'production': '',
    'development': 'http://localhost:5000'
};

function getApiServer(){

    return apiServers[process.env.NODE_ENV] || apiServers['development']
}


module.exports = {    
    getApiServer 
}