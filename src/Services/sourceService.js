import axios from 'axios';
import helpers from '../Common/helpers';

const apiServer = helpers.getApiServer();

function getPositions(){
    
    return new Promise(function(resolve, reject){
        axios.get(`${apiServer}/geo`).then(function (response) {
           
            console.log(response);
            resolve(response.data);
        }).catch(function (error) {
           
            console.log(error);
            reject(error);
        });
    });
}


function addPosition(position){
   
    return new Promise(function(resolve, reject){
       
        let requestData = {data: position};
        axios.post(`${apiServer}/geo`, requestData).then(function (response) {            
           
            console.log(response);           
            resolve(response.data.success);
        }).catch(function (error) {           
           
            console.log(error);
            reject(error);
        });
    });
}

function deletePosition(id){
    
    return new Promise(function(resolve, reject){
        
        let requestData = {params: {id: id}};
        axios.delete(`${apiServer}/geo/${id}`, requestData).then(function (response) {            
            
            console.log(response);           
            resolve(response.data.success);
        }).catch(function (error) {           
            
            console.log(error);
            reject(error);
        });
    });
}

module.exports = {
    getPositions,
    deletePosition,
    addPosition
}